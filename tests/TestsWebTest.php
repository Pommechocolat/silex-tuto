<?php
// tests/TestsWebTest.php

require_once __DIR__.'/../vendor/autoload.php';

use Silex\WebTestCase;

# Test le framework de tests fonstionnels pour vérifier le bon fonctionnement des pages (routing, etc).
class TestsWebTest extends WebTestCase {
  
  public function createApplication() {
    $app = new Silex\Application();
    $app['debug'] = true;
    $app['exception_handler']->disable();

    $app->get('/', function(){
      return 'Hello that my new world Silex';
    });

    return $app;
  }
  
  public function testInitialPage() {
    $client = $this->createClient();
    $crawler = $client->request('GET', '/');

    $this->assertTrue($client->getResponse()->isOk());
    echo "\nLa réponse en entier : \n".$client->getResponse()."\n\n";
    echo "Le contenu : \n  ".$client->getResponse()->getContent()."\n\n";
    echo "Le statut : ".$client->getResponse()->getStatusCode()."\n";
    echo "La version : ".$client->getResponse()->getProtocolVersion()."\n";
    //echo $client->getResponse()->getDate();
    //echo $client->getResponse()->getExpires();
    echo $client->getResponse()->getEtag()."\n";
    #$this->assertCount(0, $crawler->filter('h1:contains("Contact us")'));
    #$this->assertCount(0, $crawler->filter('Hello'));
  }
}