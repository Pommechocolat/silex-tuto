<?php
// tests/TestsTest.php

class TestsTest extends \PHPUnit_Framework_TestCase {
  
  public function testTestFramework() {
    //$this->assertTrue(false); #la première écriture d'un test dois planter, c'est la règle pour vérifier le fonctionnement du framework.
    $this->assertTrue(true); #la deuxième est juste là pour dire que des tests passes.
  }

  public function testArrays() {
    $stack = array();
    $this->assertEquals(0, count($stack));

    array_push($stack, 'foo');
    $this->assertEquals('foo', $stack[count($stack)-1]);
    $this->assertEquals(1, count($stack));

    $this->assertEquals('foo', array_pop($stack));
    $this->assertEquals(0, count($stack));
  }

}