<?php
// tests/UserDaoTest.php

require_once __DIR__.'/../vendor/autoload.php';

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Jlm\UL\UserDao; #Déclare l'utilisation d UserDAO provenant du paquet Jlm\UL ainsi on peut se dispenser du nom complet dans le code

class UserDaoTest extends \PHPUnit_Framework_TestCase {

    public function test_tests() {
        $this->assertEquals("0", "0", "On s'attend a avoir 0");
        $udao = null;
        $this->assertNull($udao);
        $udao = array();
        $this->assertNotNull($udao);
    }

    public function testInitialize() {
        $udao = new UserDAO();
        $this->assertNotNull($udao, "L'initialisation d'un UserDAO doit rendre un objet");
        $this->assertEquals("Jlm\\UL\\UserDAO", get_class($udao), "Le nom de la classe retrounée n'est pas bon");

        $u = $udao->loadUserByUsername("admin");
        $this->assertNotNull($u);
        $this->assertEquals("Jlm\\UL\\User", get_class($u));
        $u = $udao->loadUserByUsername("metzger9");
        $this->assertNotNull($u);
        try {
            $u = null;
            $u = $udao->loadUserByUsername("adm");
        } catch (UsernameNotFoundException $e) {
            $this->assertEquals(0, $e->getCode());
            $this->assertEquals('User "adm" not found.', $e->getMessage());
            $this->assertNull($u);
        }
    }
}