<?php
// tests/RoutingTest.php

require_once __DIR__.'/../vendor/autoload.php';

use Silex\WebTestCase;

#Test la page d'index de l'application
class RoutingTest extends WebTestCase {

    private $client = null;

    public function setUp() {
        parent::setUp();
        $this->client = static::createClient();
    }

    public function createApplication() {
        require __DIR__ . '/../src/app.php';
        $app['debug'] = true;
        $app['exception_handler']->disable();
        return $app;
    }

    public function testInitialPage() {
        echo "init testInitialPage\n";
        $crawler = $this->client->request('GET', '/');
        $this->assertTrue($this->client->getResponse()->isOk());
        $this->assertCount(1, $crawler->filter('a:contains("dossier")'));
        $a = $crawler->filter('a');
        $href = $a->extract(array('href'));
        $this->assertCount(6, $href);
        $this->assertEquals("index.php/creation", $href[0]);
    }

    // Acces sans / : il y a une redirection vers creation/
    public function testCreation() {
        echo "init testCreation\n";
        $crawler = $this->client->request('GET', 'creation');
        $this->assertEquals("302", $this->client->getResponse()->getStatusCode());
    }

    // Accès sans / devant. Ça fonctionne. Après l'ajout du controle de la page, cen'est plus bon
    public function testCreation2() {
        echo "init testCreation2\n";
        $crawler = $this->client->request('GET', 'creation/');
        $this->assertFalse($this->client->getResponse()->isOk());
        //echo "\nLa réponse en entier : \n" . $client->getResponse() . "\n\n";
    }

    // Accès avec 2 /. Ça fonctionne aussi. Après l'ajout du controle de la page, cen'est plus bon
    public function testCreation3() {
        echo "init testCreation3\n";
        $crawler = $this->client->request('GET', '/creation/');
        $this->assertFalse($this->client->getResponse()->isOk());
        //echo "\nLa réponse en entier : \n" . $client->getResponse() . "\n\n";
    }

    // Accès en POST. Ça fonction avec ou sans / devant. Après l'ajout du controle de la page, cen'est plus bon
    public function testCreationPost() {
        echo "init testCreationPost\n";
        $crawler = $this->client->request('POST', 'creation/');
        $this->assertFalse($this->client->getResponse()->isOk());
        //echo "\nLa réponse en entier : \n" . $client->getResponse() . "\n\n";
    }

    // Accès login : pas de / derrière Sans login/mdp on a un 401 : Non autorisé
    public function testLogin() {
        echo "init testLogin\n";
        $crawler = $this->client->request('GET', '/logine');
        $this->assertEquals("401", $this->client->getResponse()->getStatusCode());
        //echo "\nLa réponse en entier : \n" . $client->getResponse() . "\n\n";
    }

    //Accès sans / à login. Sans login/mdp on a un 401 : Non autorisé
    public function testLogin2() {
        echo "init testLogin2\n";
        $crawler = $this->client->request('GET', 'logine');
        $this->assertEquals("401", $this->client->getResponse()->getStatusCode());
        //echo "\nLa réponse en entier : \n" . $client->getResponse() . "\n\n";
    }

    // Une redirection vers la pacge de validation du compte (account) est opérationnel. Le test passe ainsi
    public function testLoginAvecMdP() {
        echo "init testLoginAvecMdP\n";
        // Request : http://symfony.com/fr/doc/current/cookbook/testing/http_authentication.html
        $crawler = $this->client->request('GET', 'logine', array(), array(), array(
            'PHP_AUTH_USER' => 'test',
            'PHP_AUTH_PW'   => 'test',
        ));
        $this->assertEquals("401", $this->client->getResponse()->getStatusCode());
        $crawler = $this->client->request('GET', 'logine', array(), array(), array(
            'PHP_AUTH_USER' => 'admin', #TODO le test devrait lire les variables dans le fichier de configuration en mode dev.
            'PHP_AUTH_PW'   => 'admin',
        ));
        $this->assertEquals("302", $this->client->getResponse()->getStatusCode());
        //echo "\nLa réponse en entier : \n" . $client->getResponse() . "\n\n";
        $this->assertCount(1, $crawler->filter('a:contains("administration")'));
        $crawler = $this->client->request('GET', 'administration');
        $this->assertEquals("200", $this->client->getResponse()->getStatusCode());
        $a = $crawler->filter('a');
        $href = $a->extract(array('href'));
        $this->assertCount(4, $href);
    }

    public function testLoginFormulaireMdP() {
        echo "init testLoginAvecMdP\n";
        $crawler = $this->client->request('GET', 'login');
        echo "\nLa réponse en entier : \n" . $this->client->getResponse() . "\n\n";
        $this->assertTrue($this->client->getResponse()->isOk());
    }
}