<?php
// tests/UserTest.php

require_once __DIR__.'/../vendor/autoload.php';


use Jlm\UL\User; #Déclare l'utilisation d UserDAO provenant du paquet Jlm\UL ainsi on peut se dispenser du nom complet dans le code

class UserTest extends \PHPUnit_Framework_TestCase {

    public function testInitialize() {
        $u = new User();
        $this->assertNotNull($u, "L'initialisation d'un UserDAO doit rendre un objet");
        $this->assertEquals("Jlm\\UL\\User", get_class($u), "Le nom de la classe retrounée n'est pas bon");
    }

    public function testUserAdmin() {
        $u = new User('admin', 'adminadmin');
        $pass = $u->getPassword();
        $this->assertEquals("adminadmin", $pass);
        $salt = $u->getSalt();
        $this->assertEquals("admin5", $salt);
        $uid = $u->getUsername();
        $this->assertEquals("admin", $uid);
    }


}