<?php
// src/Jlm/UL/UserDAO.php
namespace Jlm\UL;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

#cette classe permet de retourner un utilisateur afin qui puisse être d'identifier et autorisé
class UserDAO implements UserProviderInterface {
    private $listUsers;

    public function __construct($id=null, $pass=null) {
        $this->listUsers = array(
            new User('admin', 'admin'),
            new User('metzger9', 'metzger9'),
        );
    }

    public function loadUserByUsername($username) {
        $result = null;
        foreach($this->listUsers as $user) {
            if($user->getUsername()=== $username) {
                $result = $user;
                break;
            }
        }
        if(!$result) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found.', $username));
        }
        return $result;
    }

    public function refreshUser(UserInterface $user) {
        $class = get_class($user);
        if (!$this->supportsClass($class)) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', $class));
        }
        return $this->loadUserByUsername($user->getUsername());
    }

    public function supportsClass($class) {
        return 'jlm\ul\User' === $class;
    }

}