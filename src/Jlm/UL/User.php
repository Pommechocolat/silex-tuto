<?php
// src/User.php
namespace Jlm\UL;

use Symfony\Component\Security\Core\User\UserInterface;

#Il y a un User dans SécurityCore : namespace Symfony\Component\Security\Core\User;
#Mais il ne doit être utilisé que pour les user en mémoire. Ça pourrait me convenir dans un premier temps (exemple admin/admin)

# Implémante UserInerface pour permettre authentification
class User implements UserInterface
{
    private $uid;
    private $password;

    public function __construct($id=null, $pass=null) {
        $this->uid = $id;
        $this->password = $pass;
    }

    public function getRoles() {

        if($uid === "admin") {
            return array('ROLE_ADMIN','ROLE_USER');
        } else {
            return array('ROLE_USER');
        }
    }

    public function getPassword() {
        return $this->password;
    }

    public function getSalt() {
        $nb = strlen($this->uid);
        return $this->uid.$nb;
    }

    public function getUsername() {
        return $this->uid;
    }

    public function eraseCredentials() {
        // Nothing to do here
    }

}