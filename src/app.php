<?php
// src/app.php
use Symfony\Component\HttpFoundation\Response;

require_once __DIR__.'/../vendor/autoload.php';
use Silex\Provider\FormServiceProvider;

$app = new Silex\Application();

#Mise en place de la récupération des erreurs par Silex. Ne pas oublier d'ajouter la classe Response dans les use !! sinon on retrouve l'erreur dans les logs Apache.
$app->error(function (\Exception $e, $code) {
  switch ($code) {
  case 404:
    $message = 'La page demandé n\'existe pas (ou plus !).';
    break;
  default:
    $message = 'Il y a un gros problème à résoudre. Le code d\erreur est '.$code;
  }
  return new Response($message);
});

#mise en place des fournisseurs de service pour l'application. On commence avec Twig
$app->register(new Silex\Provider\SessionServiceProvider());
$app['session.test'] = true;
$app->register(new Silex\Provider\TwigServiceProvider(), array(
    'twig.path' => __DIR__.'/twig/', #composer require twig/twig
));
$app->register(new FormServiceProvider()); #composer require symfony/form, composer require symfony/security-csrf
$app->register(new Silex\Provider\TranslationServiceProvider(), array(
   'translator.messages' => array(),
)); #composer require symfony/translation
$app->register(new Silex\Provider\SecurityServiceProvider(), array(
  'security.firewalls' => array(
      'secured' => array(
          'pattern' => '^/creation',
//          'anonymous' => false,
//          'logout' => true,
          'form' => array('login_path' => '/login', 'check_path' => '/login'),
//        'users' => $app->share(function () use ($app) {
//           return new Jlm\UL\UserDAO();
//         }),
      ),
  ),
)); #composer require symfony/security

#Une fois les services en place, on peut mettre en place les routes.
require __DIR__.'/../src/routing.php';


