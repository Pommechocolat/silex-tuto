<?php
// src/routing.php

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

#Création d'une première root GET / dans une fonction anonyme (Instance de Closure)
$app->get('/', function(Request $request) {
    $baseUri = "index.php/";
    $body = 'Hello that my new world Silex';
#    $body .= "<br/>";
#    $body .= $request->getRequestUri();
#    $body .= "<br/>";
#    $body .= __DIR__;
    $body .= "<br/>";
    $body .= 'Accéder à l\'app pour créer un <a href="'.$baseUri.'creation">dossier de travaille coopératif<a/>';
    $body .= "<br/>";
    $body .= 'S\'authentifier <a href="'.$baseUri.'logine">ici<a/>';
    $body .= "<br/>";
    $body .= '<a href="'.$baseUri.'login">Login<a/>';
    return $body;
});

$form = $app['form.factory']->createBuilder('form')
    ->add('dossier')
    ->add('creer', 'submit', array('label'=>'Créer'))
    ->getForm();

#Deuxième page gérée par un controller. l'URL est index.php/creation. On voudra que cette page soit protégée par le CAS de l'université
$creation = $app['controllers_factory'];
$creation->get('/', function(Request $request) use($app, $form) {
    $mydata['dossier']= $request->get('form')['dossier'];
    print_r($mydata);
    echo '<br/>';

    return $app['twig']->render('creation.twig', array(
        'formulaire'=> $form->createView(),
    ));
});

$creation->POST('/', function(Request $request) use($app, $form) {
     return $app['twig']->render('creation.twig', array(
        'formulaire'=> $form->createView(),
    ));
});


$app->mount('/creation', $creation);

$app['redirect']="administration";

// Login form : http://silex.sensiolabs.org/doc/providers/session.html
$app->get('/logine', function(Request $request) use ($app, $form) {
    $username = $app['request']->server->get('PHP_AUTH_USER', false);
    $password = $app['request']->server->get('PHP_AUTH_PW');

    if ('admin' === $username && 'admin' === $password) { #TODO la configuration du compte d'administration doit être placée dans le fichier de configuration
        $app['session']->set('user', array('username' => $username));
        return $app->redirect($app['redirect']);
    }
    $response = new Response();
    $response->headers->set('WWW-Authenticate', sprintf('Basic realm="%s"', 'site_login'));
    $response->setStatusCode(401, 'Please sign in.');
    return $response;
});#->bind('login');  // named route so that path('login') works in Twig templates

// http://silex.sensiolabs.org/doc/providers/session.html
$app->get('/'.$app['redirect'], function () use ($app) {
    if (null === $user = $app['session']->get('user')) {
        return $app->redirect('/logine');
    }
    //print_r($app['session.storage.handler']);
    $body = "Bonjour M. {$user['username']}!";
    $body .= "<br/>";
    $body .= '<a href="..">Retour<a/>';
    $body .= "<br/>";
    $body .= '<a href="logoute">LogOut<a/>';
    return $body;
});

//La fonction de logout ne fonctionne pas !!! mais la route et la redirection oui.
$app->get('/logoute', function() use ($app) {
    return $app->redirect('..');
});

$app->get('/login', function(Request $request) use ($app) {
    return $app['twig']->render('login.html.twig', array(
//        'error'         => $app['security.last_error']($request),
//        'last_username' => $app['session']->get('_security.last_username'),
    ));
});